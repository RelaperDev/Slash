scoreboard objectives add gs_detected_tnts dummy
scoreboard objectives add gs_detected_ops dummy

execute as @a[team=!admin] run execute store success score @s gs_detected_tnts run clear @s tnt
execute as @a[team=!admin] run execute store success score @s gs_detected_tnts run clear @s tnt_minecart
execute as @a[team=!admin] run execute store success score @s gs_detected_tnts run clear @s lingering_potion
execute as @a[team=!admin] run execute store success score @s gs_detected_ops run clear @s command_block
execute as @a[team=!admin] run execute store success score @s gs_detected_ops run clear @s command_block_minecart
execute as @a[team=!admin] run execute store success score @s gs_detected_ops run clear @s chain_command_block
execute as @a[team=!admin] run execute store success score @s gs_detected_ops run clear @s repeating_command_block

execute if entity @a[team=!admin,scores={gs_detected_ops=1..}] run tellraw @a[team=admin] [{"text": "["},{"text": "GriefStriker","bold": true,"color": "green"},{"text": "] ","color": "white"},"发现下列玩家有管理员物品：",{"selector":"@a[team=!admin,scores={gs_detected_ops=1..}]"}]
execute if entity @a[team=!admin,scores={gs_detected_tnts=1..}] run tellraw @a[team=admin] [{"text": "["},{"text": "GriefStriker","bold": true,"color": "green"},{"text": "] ","color": "white"},"发现下列玩家有破坏性物品：",{"selector":"@a[team=!admin,scores={gs_detected_tnts=1..}]"}]